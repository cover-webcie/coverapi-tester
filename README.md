# Cover API Tester

A PHP script to simulate the Cover API for testing projects that depend on it.


## Usage

The easiest way to run this tester is through the PHP built-in webserver as follows.

```
cd path/to/api-tester
php -S localhost:8069
```

This will run the api-tester on http://localhost:8069/api.php. Open this link in your browser and you should see "The Cover API tester works!"

**NB** The PHP built-in webserver is single threaded, and therefore can't handle two requests at the same time. In other words, it can't do API calls to itself. If you're running your application through the built-in webserver, be sure to run the api tester in a separate process.

**NB** Make sure this script is available through the same domain as the application you're testing. For example, if you are testing ``application.svcover.localhost``, be sure to run this script on (any subdomain of) the ``svcover.localhost`` domain (e.g. ``svcover.localhost/api.php`` or ``test.svcover.localhost/api.php``). If you are testing ``localhost:8000``, run the script on any other location or port on localhost (e.g. ``localhost:8069/api.php`` or ``localhost/api.php``).

Configure the application you're testing to use this script correctly. For example, if you run the tester as ``localhost:8069/api.php``, set the following urls in your configuration

- the *Cover API URL* to ``http://localhost:8069/api.php`` (normally ``https://www.svcover.nl/api``, previously ``https://www.svcover.nl/api.php``)
- the *Cover Login URL* to ``http://localhost:8069/api.php?view=login`` (normally ``https://www.svcover.nl/login``, previously ``https://www.svcover.nl/sessions.php?view=login``)
- the *Cover Logout URL* to ``http://localhost:8069/api.php?view=logout`` (normally ``https://www.svcover.nl/logout``, previously ``https://www.svcover.nl/sessions.php?view=logout``)

Additionally, you should set the *Application name* and *Application secret* to the values defined in the API tester (default: ``test-app`` and ``ultrasecrethashkey`` respectively).

The data source used by the tester can be defined in `data.json`. Some defaults are provided in the file. It is possible to define multiple applications, members and activities (agenda). You only need to define the values you need from the API (if you don't use the calendar functions, there is no need to define activities or if you don't use a member's date of birth, you may omit it), but keep in mind to use the correct keys and formatting to ensure compatibility with the real API. Keep in mind that PHP's `json_decode` function doesn't accept comments in your JSON.

Alternatively, the data can be defined in the first few lines of `api.php`. The defaults provide there are also used in case `data.json` is not present or corrupt. 


## Considerations
This API tester tries to be as close to the real API as possible, but is not guaranteed to be a perfect match (some specialized methods and some data fields are left out). Any behavioural deviations should be considered a bug and reported in the [issue tracker](https://bitbucket.org/cover-webcie/coverapi-tester/issues). When in doubt, always consult the [original implementation](https://bitbucket.org/cover-webcie/cover-php/src/master/api.php)

The Cover API and with that also this tester contains some deprecated functionality that ensures backward compatibility. This is usually denoted with comments in the API implementation.

Please consult the AC/DCee if you've got any questions or problems with testing your application's Cover API integration.


## Postman Collection
The included Postman collection provides the reverse functionality: simulating a client. The main use case for this is easy testing new API features during development of the API itself, but it could also come in handy verifying the API's functionality while developing clients.

To use the collection, the following variables need to be defined. They can either by collection variables (set in the collection editor) or have their own environment (recommended). 

- base_url: the url the website/api tester is hosted on (default: svcover.localhost)
- app: the application name
- secret: the application secret

Due Postman not exposing the actual "raw" request body, the pre-request script renders it itself for the signing process. This should usually work, but may fail at times. Only "x-www-form-urlencoded" and "raw" request bodies are supported.