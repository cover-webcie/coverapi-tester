<?php

define('COOKIE_NAME', 'cover_session_id');

define('DEFAULT_DATA_SOURCE', [
	'applications' => ['test-app' => 'ultrasecrethashkey'],
	'committees' => [
		0 => ['board', 'Board'],
		1 => ['webcie', 'AC/DCee']
	],
	'members' => [
		709 => [
			'email' => 'jelmer@svcover.nl',
			'password' => 'test',
			'voornaam' => 'Jelmer',
			'tussenvoegsel' => 'van der',
			'achternaam' => 'Linde',
			'adres' => 'Nijenborgh 9',
			'postcode' => '9747 AG',
			'woonplaats' => 'Groningen',
			'geboortedatum' =>	'1993-09-20',
			'telefoonnummer' => '050-3636898',
			'beginjaar' => 2014,
			'nick' => 'jelmervdl',
			'member_from' => '2014-09-01',
			'member_till' => null,
			'donor_from' => null,
			'donor_till' => null,
			'committees' => [0, 1]
		]
	],
	'agenda' => [
		2222 => [
			'kop' => 'Movie night',
			'beschrijving' => "It's time for another movie night!\r\n\r\nThis time, we're going to watch The Room! We hope you're as exited as we are.\r\n\r\nPlease register [url=https://movienight.svcover.nl/]here[/url]!",
			'committee_id' => 0,
			'van' => '2022-01-01 18:00:00+00',
			'tot' => '2022-01-01 18:00:00+00', // Equal to 'van' if no end time provided
			'locatie' => 'Bernoulliborg',
			'private' => 1, // 0 (public) or 1 (means only visible for members).
			'extern' => 0, // 0 (cover activity) or 1 (external activity, eg. Beta Business Days)
		]
	]
]);


function get_data($name)
{
	static $data_source;

	if (empty($data_source)) {
		$data_source = json_decode(file_get_contents('./data.json'), true);
		
		if (empty($data_source))
			$data_source = DEFAULT_DATA_SOURCE;
	}

	return $data_source[$name];
}


class UserModel
{
	public function __construct(array $users)
	{
		$this->users = $users;
	}

	public function login($email, $password)
	{
		foreach ($this->users as $id => $user)
		{
			if ($user['email'] != $email)
				continue;

			if ($user['password'] != $password)
				return null;

			return array_merge(['id' => $id], $user);
		}

		return null;
	}

	public function get($id)
	{
		return $this->users[$id] ?? null;
	}
}


class SessionModel
{
	private function path_to_session($session_id)
	{
		return sprintf('%s/%s', sys_get_temp_dir(), $session_id);
	}

	public function create($member_id, $application)
	{
		$session_id = md5(uniqid() . strval(microtime(true)));

		$session = [
			'id' => $session_id,
			'member_id' => $member_id,
			'application' => $application,
		];

		file_put_contents($this->path_to_session($session_id), serialize($session));

		return $session;
	}

	public function resume($session_id)
	{
		if (!$session_id)
			return null;

		$file = $this->path_to_session($session_id);

		if (!file_exists($file))
			return null;

		return unserialize(file_get_contents($file));
	}

	public function delete($session)
	{
		unlink($this->path_to_session($session['id']));
	}
}


function set_domain_cookie($name, $value, $cookie_time = 0)
{
	// Determine the host name for the cookie (try to be as broad as possible so sd.svcover.nl can profit from it)
	if (preg_match('/([^.]+)\.(?:[a-z\.]{2,6})$/i', $_SERVER['HTTP_HOST'], $match))
		$domain = $match[0];
	else if ($_SERVER['HTTP_HOST'] != 'localhost')
		$domain = $_SERVER['HTTP_HOST'];
	else
		$domain = null;

	$domain = preg_replace('/:\d+$/', '', $domain);

	// If the value is empty, expire the cookie
	if ($value === null)
		$cookie_time = 1;

	$secure = !empty($_SERVER['HTTPS']);

	$http_only = true;

	setcookie($name, $value, $cookie_time, '/', $domain, $secure, $http_only);

	if ($cookie_time === 0 || $cookie_time > time())
		$_COOKIE[$name] = $value;
	else
		unset($_COOKIE[$name]);
}


class ControllerApi
{
	public function __construct()
	{
		$this->applications = get_data('applications');

		$this->committee_model = get_data('committees');

		$this->user_model = new UserModel(get_data('members'));

		$this->session_model = new SessionModel();

		$this->agenda_model = get_data('agenda');
	}


	public function api_agenda($committees=null)
	{
		if ($committees !== null && !is_array($committees))
			$committees = array($committees);

		$session = $this->session_model->resume($_GET['session_id'] ?? null);

		$activities = array_filter(
			$this->agenda_model,
			function ($value) use ($session, $committees) {
				return ($session !== null || $value['private'] === 0)
				    && ($committees === null || in_array($this->committee_model[$value['committee_id']][0], $committees));
			}
		);

		foreach ($activities as &$activity) {
			$activity['committee__naam'] = $this->committee_model[$activity['committee_id']][1];
			$activity['committee__login'] = $this->committee_model[$activity['committee_id']][0];
		}

		return $activities;
	}

	public function api_get_agendapunt()
	{
		if (empty($_GET['id']))
			throw new InvalidArgumentException('Missing id parameter');

		$activity = $this->agenda_model[$_GET['id']] ?? null;

		if (!$activity)
			throw new Exception(sprintf('Agenda with id %d was not found', $_GET['id']));

		$session = $this->session_model->resume($_GET['session_id'] ?? null);

		if ($session === null && $activity['private'] === 1)
			throw new Exception('You are not authorized to read this event');

		$activity['committee__naam'] = $this->committee_model[$activity['committee_id']][1];
		$activity['committee__login'] = $this->committee_model[$activity['committee_id']][0];
		
		return ['result' => $activity];
	}

	public function api_session_create($email, $password, $application)
	{
		if (!($member = $this->user_model->login($email, $password)))
			throw new InvalidArgumentException('Invalid username or password');

		$session = $this->session_model->create($member['id'], $application);

		return ['result' => [
			'session_id' => $session['id'],
			'details' => array_diff_key($member, ['password' => 1])
		]];
	}

	public function api_session_destroy($session_id)
	{
		$session = $this->session_model->resume($session_id);

		return $this->session_model->delete($session);
	}

	public function api_session_get_member($session_id)
	{
		$session = $this->session_model->resume($session_id);

		if (!$session)
			throw new InvalidArgumentException('Invalid session id');

		$member = $this->user_model->get($session['member_id']);

		unset($member['password']);
		$member['id'] = (int) $session['member_id'];

		$committee_data = [];

		// For now just return login and committee name
		foreach ($this->committee_model as $committee_id => $committee)
			if (in_array($committee_id, $member['committees']))
				$committee_data[$committee[0]] = $committee[1];

		return ['result' => array_merge($member, ['committees' => $committee_data])];
	}

	public function api_session_test_committee($session_id, $committees)
	{
		if (!is_array($committees))
			$committees = [$committees];

		// Get the session
		$session = $this->session_model->resume($session_id);

		if (!$session)
			throw new InvalidArgumentException('Invalid session id');

		$member = $this->user_model->get($session['member_id']);

		foreach ($committees as $committee_name)
		{
			$comm = array_filter(
				$this->committee_model,
				function ($val) use ($committee_name) 
				{	
					return in_array($committee_name, $val);
				}
			);

			if (($committee = reset($comm)) && in_array(key($comm), $member['committees']))
				return ['result' => true, 'committee' => $committee[1]];
		}

		return ['result' => false];
	}

	public function api_get_member($member_id)
	{
		$member = $this->user_model->get($member_id);

		if (!$member)
			throw new Exception(sprintf('Member with id %d was not found', $member_id));

		unset($member['password']);

		// This one is passed as parameter anyway, it is already known.
		$member['id'] = (int) $member_id;

		return ['result' => $member];
	}

	public function api_get_committees($member_id)
	{
		$member = $this->user_model->get($member_id);

		if (!$member)
			return ['result' => []];

		$committees = [];

		foreach ($member['committees'] as $committee_id)
		{
			$committee = $this->committee_model[$committee_id];	
			$committees[$committee[0]] = $committee[1];
		}

		return ['result' => $committees];
	}

	private function assert_auth_api_application()
	{
		if (empty($_SERVER['HTTP_X_APP']))
			throw new Exception('App name is missing');

		$app = $this->applications[$_SERVER['HTTP_X_APP']];

		if (!$app)
			throw new Exception('No app with that name available');

		$raw_post_data = file_get_contents('php://input');
		$post_hash = sha1($raw_post_data . $app);

		if (empty($_SERVER['HTTP_X_HASH']) || $post_hash != $_SERVER['HTTP_X_HASH'])
			throw new Exception('Checksum does not match');

		return true;
	}

	public function session_login($referer)
	{
		$error = '';
		if ($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			$member = $this->user_model->login($_POST['email'], $_POST['password']);
			if (!$member)
				$error ='Invalid username or password';
			else
			{
				$session = $this->session_model->create($member['id'], 'api');
				set_domain_cookie(COOKIE_NAME, $session['id'], time() + 24 * 3600 * 31 * 12);
				
				if (empty($referer))
				{
					echo 'Login succesful, no redirect';
					return;
				}
				
				header('Location: ' . $referer, true, 302);
				die();
			}
		}

		echo sprintf('
		<!DOCTYPE html>
		<html>
		<head>
			<title>Cover API tester login</title>
		</head>
		<body>
			<form method="POST">
				<label for="email">Email:</label>
				<input type="email" name="email">
				<label for="password">Password:</label>
				<input type="password" name="password">
				<button type="submit">Submit</button>
				<p>%s</p>
			</form>
		</html>', $error);
	}

	public function session_logout($referer)
	{
	    if (!empty($_COOKIE[COOKIE_NAME]))
	    {
			$session = $this->session_model->resume($_COOKIE[COOKIE_NAME]);
			$this->session_model->delete($session);
			set_domain_cookie(COOKIE_NAME, null);	
	    }
        
		if (empty($referer))
		{
			echo 'Logout succesful, no redirect';
			return;
		}
		
		header('Location: ' . $referer, true, 302);
		die();
	}

	public function run_impl()
	{
		$view = $_GET['view'] ?? 'api';

		switch ($view)
		{
			case 'login':
				return $this->session_login($_GET['referrer'] ?? '');

			case 'logout':
				return $this->session_logout($_GET['referrer'] ?? '');
		}

		if (empty($_GET['method'])) {
			echo 'The Cover API tester works!';
			return;
		}

		$this->assert_auth_api_application();

		switch ($_GET['method'])
		{
			// GET api.php?method=agenda[&committee[]={committee}]
			case 'agenda':
				$response = $this->api_agenda(isset($_GET['committee']) ? $_GET['committee'] : null);
				break;

			case 'get_agendapunt':
				$response = $this->api_get_agendapunt();
				break;

			// POST api.php?method=session_create
			case 'session_create':
				$response = $this->api_session_create($_POST['email'], $_POST['password'],
					isset($_POST['application']) ? $_POST['application'] : 'api');
				break;

			// POST api.php?method=session_destroy
			case 'session_destroy':
				$response = $this->api_session_destroy($_POST['session_id']);
				break;

			// GET api.php?method=session_get_member&session_id={session}
			case 'session_get_member':
				// For legacy reasons a post session id is still accepted but this method should be accessed using a GET request.
				$response = $this->api_session_get_member(empty($_POST['session_id']) ? $_GET['session_id'] : $_POST['session_id']);
				break;

			// GET api.php?method=session_test_committee&session_id={session}&committee=webcie
			case 'session_test_committee':
				// Again, legacy reasons.
				$response = $this->api_session_test_committee(
					empty($_POST['session_id']) ? $_GET['session_id'] : $_POST['session_id'],
					empty($_POST['committee']) ? $_GET['committee'] : $_POST['committee']);
				break;

			// GET api.php?method=get_member&member_id=709<&session_id=$session_id>
			case 'get_member':
				$response = $this->api_get_member($_GET['member_id']);
				break;

			// GET api.php?method=get_committees&member_id=709
			case 'get_committees':
				$response = $this->api_get_committees($_GET['member_id']);
				break;

			default:
				throw new InvalidArgumentException("Unknown method \"$method\".");
				break;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	protected function run_exception($e)
	{		
		header('Content-Type: application/json');
		echo json_encode(array('success' => false, 'error' => $e->getMessage()));
	}

	public function run()
	{
		try {
			$this->run_impl();
		} catch (Exception $e) {
			$this->run_exception($e);
		}
	}
}

$controller = new ControllerApi();
$controller->run();
